<?php
declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210614205225
 * @package DoctrineMigrations
 */
final class Version20210614205225 extends AbstractMigration
{
	/**
	 * @param \Doctrine\DBAL\Schema\Schema $schema
	 */
    public function up(Schema $schema): void
    {
		// Create 'user' table
		$table = $schema->createTable('user');
		$table->addColumn('id', 'integer', ['autoincrement'=>true]);
		$table->addColumn('email', 'string', ['notnull'=>true, 'length'=>128]);
		$table->addColumn('first_name', 'string', ['notnull'=>true, 'length'=>512]);
		$table->addColumn('last_name', 'string', ['notnull'=>true, 'length'=>512]);
		$table->addColumn('password', 'string', ['notnull'=>true, 'length'=>256]);
		$table->addColumn('date_created', 'datetime', ['notnull'=>true]);
		$table->setPrimaryKey(['id']);
		$table->addUniqueIndex(['email'], 'email_idx');
		$table->addOption('engine' , 'InnoDB');

    }

	/**
	 * @param \Doctrine\DBAL\Schema\Schema $schema
	 */
    public function down(Schema $schema): void
    {
		$schema->dropTable('user');
    }
}

<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder;

/**
 * Class RoleFixtures
 * @package App\DataFixtures
 */
class RoleFixtures extends Fixture
{
	/**
	 * @param \Doctrine\Persistence\ObjectManager $manager
	 */
    public function load(ObjectManager $manager)
    {
    	$roles = [
    		['name' => 'ROLE_USER', 'description' => 'Role for basic user'],
			['name' => 'ROLE_ADMIN', 'description' => 'Role for admin user'],
		];

    	foreach ($roles as $r) {
			$role = new Role();
			$role->setName($r['name'])
				->setDescription($r['description']);
			$manager->persist($role);

			$this->addReference($r['name'], $role);
		}
        $manager->flush();
    }
}

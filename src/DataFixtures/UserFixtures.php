<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 */
class UserFixtures extends Fixture
{
	private $userPasswordEncoder;

	/**
	 * UserFixtures constructor.
	 * @param \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface $userPasswordEncoder
	 */
	public function __construct(Encoder\UserPasswordEncoderInterface $userPasswordEncoder)
	{
		$this->userPasswordEncoder = $userPasswordEncoder;
	}

	/**
	 * @param \Doctrine\Persistence\ObjectManager $manager
	 */
    public function load(ObjectManager $manager)
    {
    	$users = [
    		['email' => 'bv@leobit.com', 'firstName' => 'Bogdan', 'lastName' => 'Vilzhanovskyy'],
			['email' => 'admin@leobit.com', 'firstName' => 'Admin', 'lastName' => 'Admin']
		];
    	foreach ($users as $u) {
			$user = new User();
			$user->setFirstName($u['firstName'])
				->setLastName($u['lastName'])
				->setEmail($u['email'])
				->setPassword($this->userPasswordEncoder->encodePassword($user, 'temp'))
				->setDateCreated(\DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s')))
				->addRole($this->getReference('ROLE_USER'));

			if($u['firstName'] === 'Admin') {
				$user->addRole($this->getReference('ROLE_ADMIN'));
			}

			$manager->persist($user);
		}
        $manager->flush();
    }

	public function getDependencies()
    {
        return [
            RoleFixtures::class,
        ];
    }

}

<?php

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class NewsFixtures
 * @package App\DataFixtures
 */
class NewsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $newsList = [
            ['title' => 'title1', 'description' => 'description1', 'content' => 'content1', 'author' => 'author1'],
            ['title' => 'title2', 'description' => 'description2', 'content' => 'content2', 'author' => 'author2'],
            ['title' => 'title3', 'description' => 'description3', 'content' => 'content3', 'author' => 'author3'],
            ['title' => 'title4', 'description' => 'description4', 'content' => 'content4', 'author' => 'author4'],
        ];
        foreach ($newsList as $n) {
            $news = new News();
            $news->setTitle($n['title'])
                ->setDescription($n['description'])
                ->setContent($n['content'])
                ->setAuthor($n['author'])
                ->setPublishDate(\DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s')));
            $manager->persist($news);
            $this->addReference($n['title'], $news);
        }

        $manager->flush();
    }
}

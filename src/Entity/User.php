<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private $password;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_created;

    /**
    * @var Collection|Role[]
    * @ORM\ManyToMany(targetEntity="Role")
    * @ORM\JoinTable(
    *      name="user_roles",
    *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
    * )
     */
    private $roles;

    private $user_roles;

	/**
	 * @return int|null
	 */
    public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @return string|null
	 */
    public function getEmail(): ?string
    {
        return $this->email;
    }

	/**
	 * @param string $email
	 * @return $this
	 */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

	/**
	 * @return string|null
	 */
    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

	/**
	 * @param string $first_name
	 * @return $this
	 */
    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

	/**
	 * @return string|null
	 */
    public function getLastName(): ?string
    {
        return $this->last_name;
    }

	/**
	 * @param string $last_name
	 * @return $this
	 */
    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

	/**
	 * @return string|null
	 */
    public function getPassword(): ?string
    {
        return $this->password;
    }

	/**
	 * @param string $password
	 * @return $this
	 */
    public function setPassword(string $password): self
    {

        $this->password = $password;

        return $this;
    }

	/**
	 * @return \DateTimeInterface|null
	 */
    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->date_created;
    }

	/**
	 * @param \DateTimeInterface $date_created
	 * @return $this
	 */
    public function setDateCreated(\DateTimeInterface $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }

	/**
	 * @return string|null
	 */
	public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * @return int
     */
	public function getUserIdentifier(): int
    {
        return $this->getId();
    }

	public function getSalt(){}

	public function eraseCredentials(){}

    /*
    * @param Role $role
    * @return $this
    */
    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /*
    * @param Role $role
    * @return $this
    */
    public function removeRole(Role $role): self
    {
        $this->roles->removeElement($role);

        return $this;
    }

    /*
    * @retrun array
    */
    public function getRoles(): array {
        $roles = [];
        foreach($this->roles as $role) {
            $roles[] = $role->getName();
        }

        return $roles;
    }

        /*
    * @param Role $role
    * @return $this
    */
    public function addUserRoles(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /*
    * @param Role $role
    * @return $this
    */
    public function removeUserRoles(Role $role): self
    {
        $this->roles->removeElement($role);

        return $this;
    }

    public function getUserRoles()
    {
        return $this->roles;
    }
}

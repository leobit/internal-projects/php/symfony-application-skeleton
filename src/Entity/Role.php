<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\RoleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RoleRepository::class)
 */
class Role
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128, unique=true, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

	/**
	 * @return int|null
	 */
    public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
    public function getName(): string
    {
        return $this->name;
    }

	/**
	 * @param string $name
	 * @return $this
	 */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
	 * @return string|null
	 */
    public function getDescription(): ?string
    {
        return $this->description;
    }

	/**
	 * @param string $description
	 * @return $this
	 */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}

## Setting up example project
```bash
$ git clone 
$ docker-compose up --build -d
$ composer install
$ php bin/console doctrine:migrations:migrate
$ php bin/console doctrine:fixtures:load
$ npm run watch
```

## MIGRATION GENERATE
```bash
$ php bin/console doctrine:migrations:generate
```

## MIGRATION REVERT
```bash
$ php bin/console doctrine:migrations:execute --down 'DoctrineMigrations\Version20210614205225'
```

## MIGRATION execute only one
```bash
$ php bin/console doctrine:migrations:execute --up 'DoctrineMigrations\Version20210614205225'
```

## MIGRATION BUILD RUN
```bash
$ php bin/console doctrine:migrations:migrate
```

## MIGRATION STATUS
```bash
$ php bin/console doctrine:migrations:status
```

## [CREATE ENTITY](https://symfony.com/doc/current/doctrine.html#creating-an-entity-class) 
```bash
$ php bin/console make:entity
```

## Auto create a migration from existing entity
```bash
$ php bin/console make:migration
```

## Make form 
```bash
$ php bin/console make:form
```

## Make Fixtures
```bash
$ php bin/console make:fix
```

## Load your Fixtures by running
```bash
$ php bin/console doctrine:fixtures:load
```

## Create Controller
```bash
php bin/console make:controller UserController
```

## Install dependencies
```bash
composer install
npm install
```

## compile assets once
```bash
npm run dev
```

## recompile assets automatically
```bash
npm run watch
```

## create production build
```bash
npm run build
```
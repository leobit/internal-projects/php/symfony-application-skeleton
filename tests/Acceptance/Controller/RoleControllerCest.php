<?php
declare(strict_types=1);

class RoleControllerCest
{
    public function testIndex(\AcceptanceTester $I)
    {
        $I->wantTo('Test the role index page');

        $I->login($I);

        $I->amOnPage('/roles');
        $I->seeResponseCodeIs(200);
        $I->see('Manage Roles');
    }

    public function testShow(\AcceptanceTester $I)
    {
        $I->wantTo('Test the role show page');

        $I->login($I);

        $I->amOnPage('/roles/1');
        $I->seeResponseCodeIs(200);
        $I->see('ROLE_USER');
    }

    public function testNew(\AcceptanceTester $I)
    {
        $I->wantTo('Test the creation role');

        $I->login($I);

        $I->amOnPage('/roles/new');
        $I->seeResponseCodeIs(200);
        $I->see('Create new Role');
        $I->fillField('role[name]', 'ROLE_TEST');
        $I->fillField('role[description]', 'RoleDescription');
        $I->click('Save');
        $I->see('ROLE_TEST');
    }

    public function testEdit(\AcceptanceTester $I)
    {
        $I->wantTo('Test the edit role');

        $I->login($I);
        
        $I->amOnPage('/roles');
        $I->seeResponseCodeIs(200);
        $I->see('Manage Roles');
        $I->click('Edit' , \Codeception\Util\Locator::elementAt('.btn-info', -1));
        $I->see('Edit Role');
        $I->seeInField('role[name]', 'ROLE_TEST');
        $I->seeInField('role[description]', 'RoleDescription');
        $I->fillField('role[name]', 'ROLE_TEST2');
        $I->fillField('role[description]', 'RoleDescription2');
        $I->click('Update');
        $I->see('Manage Roles');
        $I->see('ROLE_TEST2');
        $I->see('RoleDescription2');
    }

    
    public function testDelete(\AcceptanceTester $I)
    {
        $I->wantTo('Test the delete role');

        $I->login($I);
        
        $I->amOnPage('/roles');
        $I->seeResponseCodeIs(200);
        $I->see('Manage Roles');
        $I->click('Delete' , \Codeception\Util\Locator::elementAt('.btn-danger', -1));
        $I->see('Role');
        $I->see('ROLE_TEST2');
        $I->click('Delete');
        $I->see('Manage Roles');
        $I->dontSee('ROLE_TEST2');
        $I->dontSee('RoleDescription2');
    }
}

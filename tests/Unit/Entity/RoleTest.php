<?php
declare(strict_types=1);

use App\Entity\Role;
use Codeception\Test\Unit;
use Doctrine\ORM\EntityManagerInterface;

class RoleTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected function _before()
    {
        $this->entityManager = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testRoleEntity()
    {
        $role = new Role();

        $role->setName('TestRole');
        $role->setDescription('TestDescription');

        $this->assertEquals(null, $role->getId());
        $this->assertEquals('TestRole', $role->getName());
        $this->assertEquals('TestDescription', $role->getDescription());
    }

    public function testSetName()
    {
        $role = new Role();

        $role->setName('TestRole');

        $this->assertEquals('TestRole', $role->getName());
    }

    public function testSetDescription()
    {
        $role = new Role();

        $role->setDescription('TestDescription');

        $this->assertEquals('TestDescription', $role->getDescription());
    }
}

<?php
declare(strict_types=1);

use App\Entity\Role;
use App\Form\RoleType;
use Symfony\Component\Form\Test\TypeTestCase;

class RoleTypeTest extends TypeTestCase
{

    public function testSubmitValidData()
    {
        $formData = [
            'name' => 'TestRole',
            'description' => 'TestDescription',
        ];

        $objectToCompare = new Role();
        $form = $this->factory->create(RoleType::class, $objectToCompare);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($objectToCompare, $form->getData());
        $this->assertEquals('TestRole', $objectToCompare->getName());
        $this->assertEquals('TestDescription', $objectToCompare->getDescription());
    }
}

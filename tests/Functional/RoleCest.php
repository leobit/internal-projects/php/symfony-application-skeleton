<?php
declare(strict_types=1);

class RoleCest
{
    public function testRegularUserLogin(\FunctionalTester $I)
    {
        $I->wantTo('Test regular user login');

        $I->amOnPage('/login');
        $I->fillField('email', 'bv@leobit.com');
        $I->fillField('password', 'temp');
        $I->click('Sign in');
        $I->see('You are logged in as bv@leobit.com');
        $I->dontSee('Roles');

        $I->amOnPage('/roles');
        $I->seeResponseCodeIs(403);
    }

    public function testAdminUserLogin(\FunctionalTester $I)
    {
        $I->wantTo('Test regular user login');

        $I->amOnPage('/login');
        $I->fillField('email', 'admin@leobit.com');
        $I->fillField('password', 'temp');
        $I->click('Sign in');
        $I->see('You are logged in as admin@leobit.com');
        $I->see('Roles');

        $I->amOnPage('/roles');
        $I->seeResponseCodeIs(200);
        $I->see('Manage Roles');
    }
}

<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{

    public function login(\AcceptanceTester $I)
    {
        $I->amOnPage('/login');
        $I->fillField('email', 'admin@leobit.com');
        $I->fillField('password', 'temp');
        $I->click('Sign in');
        $I->see('You are logged in as admin@leobit.com');
    }

}
